import java.util.*;

class Solution3{
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter a number :");
		int num = sc.nextInt();
		int temp = num;
		int sum= 0;
		while(num>0){
			int fact = 1;
			int rem = num%10;
				for (int i =1; i<=rem;  i++){
					fact = fact *i;
				}
			num= num/10;
			sum= sum+ fact;
		} 
		if (sum==temp ) {
			System.out.println(" Given number is strong number");
		} else {
			System.out.println("Given number is not strong number");
		}
	}
}

