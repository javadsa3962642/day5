import java.util.*;

class Solution2{
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter number of rows: ");
		int row= sc.nextInt();


		for(int i = 1;  i<=row; i++){
			char ch = 'a';                                                                                                                                                              char ch1 = 'A'; 
                        
			for(int j= 1;  j<=i;  j++){
				if (i%2==1){
					System.out.print(ch++  + " ");
				} else {
					System.out.print(ch1++  + " ");
				}
			}
			System.out.println();
		}
	}
}
