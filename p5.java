import java .util.*;
class Solution5{
	public static void main(String [] args){
		Scanner sc= new Scanner ( System.in);
		System.out.print ("Enter a string :");
		String str = sc.nextLine().toLowerCase();
		
		System.out.print("Enter the letter to count: ");
	   char letterCount = sc.nextLine().toLowerCase().charAt(0);

			int count = 0;
		        for (int i = 0; i < str.length(); i++) {
			            if (str.charAt(i) == letterCount) {
				                    count++;
			                }
			            }
				        System.out.println("Alphabet: " + letterCount);
				        System.out.println("Output: " + count);
	    }
}

