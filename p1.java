import java.util.*;

class Solution1{
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter starting number :");
		int start = sc.nextInt();
		System.out.print("Enter ending number: ");
		int end = sc.nextInt();
		int fact = 1;
		for (int i = start;  i<=end;  i++){
			fact = fact*i;
		} 
		System.out.println(fact +  " ");
	}
}
